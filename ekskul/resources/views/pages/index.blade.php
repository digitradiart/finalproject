<!-- SCRIPT BY AYU AGUSTINA, digitradiart@gmail.com -->

@extends('layouts.master')

@section('title')
    Halaman Utama
@endsection

@section('subtitle')
    Utama
@endsection

@section('content')
<body>
    <h1>KelasEkskul</h1> <hr>
    <h2>Wadah untuk mengembangkan Potensimu</h2>
    <p>Belajar dan berbagi agar hidup ini semakin berkualitas</p>

    <h3>Benefit Join di KelasEkskul</h3>
    <ul>
        <li>Mendapatkan motivasi dari sesama anggota</li>
        <li>Sharing knowledge dari para mastah</li>
        <li>Bertemu teman-teman dengan minat yang sama</li>
        <li>Wadah untuk menggali potensi minat</li>
    </ul>

    <h3>Cara Bergabung di KelasEkskul</h3>
    <ol>
        <li>Mengunjungi website ini</li>
        <li>
            Mendaftar di <a href="/register" target="_blank"> Form Sign Up</a>
        </li>
        <li>Selesai!</li>
    </ol>
</body>
@endsection
