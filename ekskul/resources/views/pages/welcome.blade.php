<!-- SCRIPT BY AYU AGUSTINA, digitradiart@gmail.com -->
@extends('layouts.master')

@section('title')
    Halaman Utama
@endsection

@section('subtitle')
    Utama
@endsection

@section('content')
    <h1>Selamat Datang, {{$namaDepan}} {{$namaBelakang}}!</h1>
    <h2>Terima kasih telah bergabung di KelasEkskul.</h2>
    <h2>Kembangkan potensimu bersama kami di sini.</h2>
@endsection
